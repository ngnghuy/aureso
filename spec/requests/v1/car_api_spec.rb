require 'rails_helper'

RSpec.describe 'AuresoAPI::V1::Cars', type: :request do
  describe 'GET /api/v1/cars/#{car_slug} ' do
    let(:endpoint) { "/api/v1/cars/#{car_slug}" }

    context 'Abnormal system' do
      let(:car_slug) { "abc" }
      before do
        get endpoint
        @json = JSON.parse(response.body)
      end

      it 'should return not found with status 404' do
        expect(@json['status']).to eq(Settings.errors.not_found.status)
        expect(@json['message']).to eq(Settings.errors.not_found.message)
      end
    end
    
    context 'Normal system' do
      let(:car) { create(:car) }
      let(:track) { create(:track)}
      let(:car_slug) { car.car_slug }
      before do
        get endpoint
        @json = JSON.parse(response.body)
      end

      it 'should return the status code 200' do
        expect(response.status).to eq(200)
      end

      shared_examples 'return present data' do |key|
        it "should return '#{key}' in the response" do
          expect(@json['car'].key?(key)).to eq(true)
          expect(@json['car'][key].present?).to eq(true)
        end
      end

      %w[id car_slug max_speed max_speed_on_track].each do |key|
        it_behaves_like 'return present data', key.to_s
      end
    end
  end
end
