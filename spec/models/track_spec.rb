require 'rails_helper'

RSpec.describe Track, type: :model do
  describe 'Track' do
    before do
      @track = FactoryBot.build(:track)
    end

    subject { @track }

    it { should respond_to(:name) }
    it { should respond_to(:surface_type) }
    it { should be_valid }

  end
end
