require 'rails_helper'

RSpec.describe Car, type: :model do
  describe 'Care' do
    before do
      @car = FactoryBot.build(:car)
    end

    subject { @car }

    it { should respond_to(:name) }
    it { should respond_to(:car_slug) }
    it { should respond_to(:max_speed) }

    it { should be_valid }

    it { should validate_uniqueness_of(:car_slug) }

  end
end
