FactoryBot.define do
  factory :car do
    name { "MyString" }
    car_slug { "MyString" }
    max_speed { 1 }
  end
end
