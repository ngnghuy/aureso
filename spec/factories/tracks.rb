FactoryBot.define do
  factory :track do
    name { "MyString" }
    surface_type { :snow }
    time_zone_area { Time.zone.name }
  end
end
