class AddTimeZoneAreaToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :time_zone_area, :string
  end
end
