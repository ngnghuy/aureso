class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :name
      t.string :car_slug
      t.integer :max_speed

      t.timestamps null: false
    end
    add_index :cars, :car_slug, unique: true
  end
end
