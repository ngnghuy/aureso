# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Car.create(name: 'Honda', car_slug: 'honda_car', max_speed: 160)

Track.create(name: 'Nurburgring', surface_type: :snow, time_zone_area: 'Lisbon')
Track.create(name: 'Sydney Motorsport Park', surface_type: :snow, time_zone_area: 'Sydney')
Track.create(name: 'Circuit Gilles Villenaeuve Montreal', surface_type: :snow, time_zone_area: 'Eastern Time (US & Canada)')
Track.create(name: 'Guia Circut', surface_type: :snow, time_zone_area: 'Hong Kong')
