if defined? GrapeSwaggerRails
  GrapeSwaggerRails.options.app_name    = 'Aureso API'
  GrapeSwaggerRails.options.app_url     = '/'
  GrapeSwaggerRails.options.url         = 'swagger_doc'
end