class Car < ActiveRecord::Base
	validates :car_slug, uniqueness: true

  def max_speed_on_track(track)
  	factors = [ track.surface_type.value, track.current_driving_time ]
  	percentage = factors.sum
  	slowing_factor =  percentage / (max_speed*100).to_f
    (max_speed - slowing_factor).to_f
  end
end