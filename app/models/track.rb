class Track < ActiveRecord::Base
	extend Enumerize

	enumerize :surface_type, in: { snow: 35, gravel: 12, asphalt: 20 }, scope: true

	TIME_FACTORS = [
		{
			from: '9am',
			to: '6pm',
			lost_speed: 0
		},
		{
			from: '6pm',
			to: '9:30pm',
			lost_speed: 8
		},
		{
			from: '9:30pm',
			to: '6am',
			lost_speed: 15
		},
		{
			from: '6am',
			to: '9am',
			lost_speed: 8
		},
	]

	def current_driving_time
		TIME_FACTORS.each do |t|
			from = Time.parse(t[:from]).in_time_zone(time_zone_area)
			to = Time.parse(t[:to]).in_time_zone(time_zone_area)
			current = Time.current.in_time_zone(time_zone_area)
			time_frame = current.between?(from, to)
			return time_frame ? t[:lost_speed] : 0
		end
	end

	class << self
		def find_by_name(name)
			return 'no track selected' if name.blank?
			track = Track.find_by(name: name)
			return 'track not found' if track.blank?
			track
		end
	end
end
