module AuresoAPI
  class Root < Grape::API
    prefix :api
    mount V1::Root
  end
end
