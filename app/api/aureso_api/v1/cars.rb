module AuresoAPI
  module V1
    class Cars < Grape::API
      namespace :cars do
        desc 'Get car information by car_slug'
        params do
          requires :car_slug, type: String, desc: 'Car slug'
          optional :track, type: String, desc: 'Track type'
        end

        get ':car_slug' do
          car = Car.find_by(car_slug: params[:car_slug])
          track = Track.find_by_name(params[:track])
          response!(Settings.errors.not_found) if car.nil?
          present car, with: AuresoAPI::V1::Entities::CarDetail, track: track
        end
      end
    end
  end
end
