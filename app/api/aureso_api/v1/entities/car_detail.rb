module AuresoAPI
  module V1
    module Entities
      class CarDetail < Grape::Entity
        expose :car do
          expose :id
          expose :car_slug
          expose :max_speed do |car|
            "#{car.max_speed}km/h"
          end
          expose :max_speed_on_track do |car, options|
            track = options[:track]
            track.try(:name).present? ? "#{car.max_speed_on_track(track).round(3)}km/h" : track
          end
        end
      end
    end
  end
end
