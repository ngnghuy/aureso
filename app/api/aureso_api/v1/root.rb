module AuresoAPI
  module V1
    module ErrorFormatter
      def self.call(error, _backtrace, _options, _env, _original_exception)
        case error
        when Hash
          {
            status: error[:status],
            message: error[:message]
          }
        else
          { data: error }
        end.merge(timestamp: Time.now.to_i).to_json
      end
    end

  	class Root < Grape::API
  		version 'v1', using: :path
      format :json
      error_formatter :json, ErrorFormatter

      helpers do
        def response!(error, args = {})
          message = error.message
          message = "#{message} (#{args[:detail_message]})" if args[:detail_message]
          Rails.logger.error "API ERROR[#{error.status}] #{message}"
          error!({ status: error.status, message: message }, error.status)
        end
      end

      rescue_from ActiveRecord::UnknownAttributeError do |e|
        response!(
          Settings.errors.unprocessable_entity,
          detail_message: e.message
        )
      end

      rescue_from ActiveRecord::RecordInvalid do |e|
        response!(Settings.errors.unprocessable_entity, detail_message: e.message)
      end

      rescue_from ActiveRecord::RecordNotFound do |e|
        response!(Settings.errors.not_found, detail_message: e.message)
      end


      mount Cars
  	end
  end
end
