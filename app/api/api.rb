module API
  class Root < Grape::API
    mount AuresoAPI::Root

    add_swagger_documentation
  end
end